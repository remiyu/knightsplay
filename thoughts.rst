Brainstorming
=============

Player Units
------------

Stats
- int (inc magic; some mod on skill dps)
- str (inc weapon dmg; more str for bigger weapons)
- vit (inc hp)
- agi (inc dodge; inc crit)
- HP/Magic/Exp
- Dodge/block
- defensive resists
- offensive boosts (+dmg vs enemy types)
- Hidden stats?
  - MF/GF/Speed? (aka modifier on initiation rolls)
    
Classes
- Adventurer (even stat distrib)
- Rogue (agi; low int)
- Warrior (str; low int)
- Wizard (int; low str)
  
Gear slots
- helm
- 2x ring
- neck
- chest
- boots
- main-hand (weapon)
- off-hand (shield, source, OH weapon)

Skills
- Attack, defend, two active skills, escape
- skills unlock at higher levels
- skill points used to learn and upgrade
- active/passive skills
  
Level up
- n stat points per level
- 1 skill point
- Fill hp/magic
- increase by x% to achieve next lvl

Status effects
- Buffs/Debuffs


Enemy Units
-----------

TODO


Items
-----

TODO
